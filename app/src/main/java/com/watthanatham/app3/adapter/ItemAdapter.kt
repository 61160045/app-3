package com.watthanatham.app3.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.watthanatham.app3.MainActivity
import com.watthanatham.app3.R
import com.watthanatham.app3.model.Oil

class ItemAdapter(private val context: MainActivity, private val dataset: List<Oil>) : RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {
    class ItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        val price: TextView = view.findViewById(R.id.textPrice)
        val name: TextView = view.findViewById(R.id.textName)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context).inflate(R.layout.list, parent, false)
        return ItemViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.price.text = dataset[position].price.toString()
        holder.name.text = dataset[position].name
    }

    override fun getItemCount(): Int {
       return dataset.size
    }
}