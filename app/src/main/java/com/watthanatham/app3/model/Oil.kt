package com.watthanatham.app3.model

import androidx.annotation.StringRes

data class Oil(val name: String, val price: Double)
