package com.watthanatham.app3.data

import com.watthanatham.app3.model.Oil

class Datasource {
    fun loadDataOil() : List<Oil> {
        return listOf<Oil>(
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ E20", 37.24),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 91", 38.08),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 95", 38.35),
            Oil("เชลล์ วี-เพาเวอร์ แก๊สโซฮอล์ 95", 45.84),
            Oil("เชลล์ ดีเซล B20", 36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล", 36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล B7", 36.34),
            Oil("เชลล์ วี-เพาเวอร์ ดีเซล", 36.34),
            Oil("เชลล์ วี-เพาเวอร์ ดีเซล B7", 47.06)
        )
    }
}